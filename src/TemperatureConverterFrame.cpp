#include "TemperatureConverterFrame.hpp"

#include <cstdlib>

#include "TemperatureConverter.hpp"

wxBEGIN_EVENT_TABLE(TemperatureConverterFrame, wxFrame)
    EVT_MENU(wxID_EXIT, TemperatureConverterFrame::OnExit)

    EVT_TEXT_ENTER(ID_KELVIN_ENTRY, TemperatureConverterFrame::ConvertKelvin)
    EVT_TEXT_ENTER(ID_CELSIUS_ENTRY, TemperatureConverterFrame::ConvertCelsius)
    EVT_TEXT_ENTER(ID_FAHRENHEIT_ENTRY, TemperatureConverterFrame::ConvertFahrenheit)
wxEND_EVENT_TABLE()

TemperatureConverterFrame::TemperatureConverterFrame() : wxFrame(nullptr, wxID_ANY, _("Temperature Converter"), wxPoint(50, 50), wxSize(450, 340)), m_temperature{0} {
    SetupMenuBar();

    SetupContents();
}

#define wxTEXT_CTRL(WIN, ID, FLAGS) wxTextCtrl(WIN, ID, wxEmptyString, wxDefaultPosition, wxDefaultSize, FLAGS)

void TemperatureConverterFrame::SetupContents() {
    m_sizer = new wxBoxSizer(wxVERTICAL);

    m_kelvinEntry = new wxTEXT_CTRL(this, ID_KELVIN_ENTRY, wxTE_PROCESS_ENTER);
    m_sizer->Add(m_kelvinEntry, 0, 0, 0);

    m_celsiusEntry = new wxTEXT_CTRL(this, ID_CELSIUS_ENTRY, wxTE_PROCESS_ENTER);
    m_sizer->Add(m_celsiusEntry, 0, 0, 0);

    m_fahreheitEntry = new wxTEXT_CTRL(this, ID_FAHRENHEIT_ENTRY, wxTE_PROCESS_ENTER);
    m_sizer->Add(m_fahreheitEntry, 0, 0, 0);

    m_sizer->SetSizeHints(this);

    SetSizer(m_sizer);
}

void TemperatureConverterFrame::SetupMenuBar() {
    wxMenu *menuFile = new wxMenu;
    menuFile->Append(wxID_EXIT);

    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append(menuFile, _("&File"));

    SetMenuBar(menuBar);

    Bind(wxEVT_MENU, &TemperatureConverterFrame::OnExit, this, wxID_EXIT);
}

void TemperatureConverterFrame::ConvertKelvin(wxCommandEvent&) {
    double value = atof(m_kelvinEntry->GetValue());

    m_temperature = Temperature{value, Temperature::Unit::kelvin};
    UpdateFields();
}

void TemperatureConverterFrame::ConvertCelsius(wxCommandEvent&) {
    double value = atof(m_celsiusEntry->GetValue());

    m_temperature = Temperature{value, Temperature::Unit::celsius};
    UpdateFields();
}

void TemperatureConverterFrame::ConvertFahrenheit(wxCommandEvent&) {
    double value = atof(m_fahreheitEntry->GetValue());

    m_temperature = Temperature{value, Temperature::Unit::fahrenheit};
    UpdateFields();
}

void TemperatureConverterFrame::UpdateFields() {
    m_kelvinEntry->SetValue(wxString::Format(_("%.2f"), m_temperature.kelvinValue()));
    m_celsiusEntry->SetValue(wxString::Format(_("%.2f"), m_temperature.celsiusValue()));
    m_fahreheitEntry->SetValue(wxString::Format(_("%.2f"), m_temperature.fahrenheitValue()));
}

void TemperatureConverterFrame::OnExit(wxCommandEvent&) {
    Close(true);
}

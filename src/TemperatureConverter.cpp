#include "TemperatureConverter.hpp"

inline double celsiusToKelvin(double celsius) {
    return celsius + 273.15;
}

inline double fahrenheitToKelvin(double fahrenheit) {
    return 5./9 * (fahrenheit - 32) + 273.15;
}

double Temperature::kelvinValue() {
    return m_kelvinValue;
}

double Temperature::celsiusValue() {
    return m_kelvinValue - 273.15;
}

double Temperature::fahrenheitValue() {
    return 32 + (m_kelvinValue - 273.15) * 1.8;
}

Temperature::Temperature(double kelvinValue) : m_kelvinValue{kelvinValue} {}

Temperature::Temperature(double value, Unit unit) {
    switch (unit) {
        case Unit::kelvin:
            m_kelvinValue = value;
            break;
        case Unit::celsius:
            m_kelvinValue = celsiusToKelvin(value);
            break;
        case Unit::fahrenheit:
            m_kelvinValue = fahrenheitToKelvin(value);
            break;
    }
}

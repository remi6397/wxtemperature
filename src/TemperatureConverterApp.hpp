
#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "TemperatureConverterFrame.hpp"

class TemperatureConverterApp : public wxApp {
public:
    virtual bool OnInit();
};

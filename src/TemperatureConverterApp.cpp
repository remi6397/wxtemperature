#include "TemperatureConverterApp.hpp"

wxIMPLEMENT_APP(TemperatureConverterApp);

bool TemperatureConverterApp::OnInit() {
    TemperatureConverterFrame *frame = new TemperatureConverterFrame();
    frame->Show(true);

    return true;
}

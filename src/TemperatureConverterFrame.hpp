#pragma once

#include "TemperatureConverterApp.hpp"
#include "TemperatureConverter.hpp"

class TemperatureConverterFrame : public wxFrame {
public:
    TemperatureConverterFrame();

private:
    void SetupMenuBar();
    void SetupContents();

    void OnExit(wxCommandEvent&);

    void ConvertKelvin(wxCommandEvent&);
    void ConvertCelsius(wxCommandEvent&);
    void ConvertFahrenheit(wxCommandEvent&);

    void UpdateFields();

    wxBoxSizer *m_sizer;

    wxTextCtrl *m_kelvinEntry;
    wxTextCtrl *m_celsiusEntry;
    wxTextCtrl *m_fahreheitEntry;

    Temperature m_temperature;

    wxDECLARE_EVENT_TABLE();
};

enum {
    ID_KELVIN_ENTRY = 101,
    ID_CELSIUS_ENTRY = 102,
    ID_FAHRENHEIT_ENTRY = 103
};

#pragma once

class Temperature {
public:
    enum class Unit { kelvin, celsius, fahrenheit };

    Temperature(double kelvinValue);
    Temperature(double value, Unit unit);

    double kelvinValue();
    double celsiusValue();
    double fahrenheitValue();

private:
    double m_kelvinValue;
};
